## nand2tetris

* https://www.nand2tetris.org/

## RISC-V

* [RISC-V 手册 一本开源指令集的指南 (PDF)](http://crva.ict.ac.cn/documents/RISC-V-Reader-Chinese-v2p1.pdf)
    * https://github.com/geohot/twitchcore
* [從 RISC-V 處理器到 UNIX 作業系統](https://github.com/riscv2os/riscv2os/wiki)

## RISC-V 課程

* 課程 -- https://passlab.github.io/CSCE513/#schedule (讚，投影片做得很好，可上課用)
    * 教學影片 -- https://www.youtube.com/playlist?list=PL5z7yDpfpfNcnpyX68BlejzWsk3PH7tc- (但這個影片是 MIPS 版的)
    * CSCE 513 Computer Architecture, Fall 2018
    * University of South Carolina
    * [RISC-V Single-Cycle Implementation (PDF)](https://passlab.github.io/CSCE513/notes/lecture07_RISCV_Impl.pdf)
    * 使用 : Computer Organization and Design, RISC-V Edition: The Hardware/Software Interface 作為參考書
    * Computer Architecture A Quantitative Approach, 6th Edition, John L. Hennessy and David A. Patterson 作為教科書

## RISC-V 實作

* https://github.com/ucb-bar/riscv-sodor (官方，用 Chisel 語言)



* https://github.com/stevehoover/RISC-V_MYTH_Workshop (讚)
    * https://www.vlsisystemdesign.com/riscv-based-myth/
    * https://www.makerchip.com/
    * https://www.makerchip.com/sandbox/
    * For students of "Microprocessor for You in Thirty Hours" Workshop, offered by for VLSI System Design (VSD) and Redwood EDA, find here accompanying live info and links for Day 3 - 5.
* https://github.com/ESEO-Tech/emulsiV
    * emulsiV is a visual simulator for Virgule, a minimal CPU core implementation based on the RISC-V architecture. This simulator is intended to be used as a tool for teaching the basics of computer architecture.

## Computer Organization

* https://cs61c.org/
    * CS 61C Fall 2022 -- Great Ideas in Computer Architecture (Machine Structures) at UC Berkeley with Dan Garcia, Lisa Yan 

## Computer Science

* CS208E Great Ideas in Computer Science
    * https://web.stanford.edu/class/cs208e/

# reference2

* https://www.cs.cornell.edu/courses/cs3410/2019sp/schedule/ (讚)(RISC-V 就用這版)
    * 習題 -- https://www.cs.cornell.edu/courses/cs3410/2019sp/projects/alu/
    * [Group Project 2 - Pipelined Mini-RISC-V](https://www.cs.cornell.edu/courses/cs3410/2019sp/projects/mini-risc/)
    * [Group Project 3 - Full RISC-V](https://www.cs.cornell.edu/courses/cs3410/2019sp/projects/full-risc/)
    * https://github.com/Strafos/CS_3410
    * https://github.com/scutdig/Digiblock (視覺化電路設計工具)

* https://github.com/ultraembedded/riscv
    * 這個是用 Verilog 設計 ， SystemC 寫測試，然後用 VERILATOR 跑的

* [Computer Organization and Design RISC-V Edition (PDF)](http://home.ustc.edu.cn/~louwenqi/reference_books_tools/Computer%20Organization%20and%20Design%20RISC-V%20edition.pdf)
    * 

* [11001 資訊工程學系 計算機結構(高畫質版=10002)](https://ocw.nthu.edu.tw/ocw/index.php?page=course&cid=305&)
    * [Designing a Single-Cycle Processor](https://ocw.nthu.edu.tw/ocw/upload/305/7305/Single-Cycle%20Processor%20.pdf)
    * https://hackmd.io/@sysprog/cpu-arch-lecture?type=view
    * 計算機結構課程 黃婷婷教授的 Computer Architecture 課程錄影 
    * 參考資料：http://www.ece.lsu.edu/ee4720/
    * https://www.ece.lsu.edu/ee4720/
    * MIPS Verilog 實作 -- https://www.ece.lsu.edu/ee4720/v/index.html
