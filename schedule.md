# Schedule

* 1-6 周 nand2tetris
* 7 : Computer Speed
* 8 : cs3410
    * [02-Logic & Gates](https://www.cs.cornell.edu/courses/cs3410/2019sp/schedule/slides/02-gates-notes.pdf) (62 頁)
    * [03-Numbers & Arithmetic](https://www.cs.cornell.edu/courses/cs3410/2019sp/schedule/slides/03-numbers-and-arithmetic-notes.pdf) (67 頁)
    * [04-State](https://www.cs.cornell.edu/courses/cs3410/2019sp/schedule/slides/04-state-notes.pdf) (37頁)